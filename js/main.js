// createDayDropdown ();
// createMonthDropdown ();
// createYearDropdown();

$(document).ready(function() {
    // initialize calendar
    $('#deadline').datepicker({
        dateFormat: "yy-mm-dd",
        minDate: '+1d'
      });

    $('#actionFilter').change(function(){

        // Clear data from html DOM
        const ul = document.querySelector('.cont_princ_lists > ul');
        ul.innerHTML = '';

        // Load already saved data
        retrieve_data(create_filter());
    });

    $('#dateFilter').change(function(){
         // Clear data from html DOM
         const ul = document.querySelector('.cont_princ_lists > ul');
         ul.innerHTML = '';
         
        retrieve_data(create_filter());
    });

    
    let test;
    // Load already saved data
    retrieve_data({});

    setTimeout(function(){
        console.log('TEST: ' + test);
    },1000);
    
});

function create_filter(){
    let filter = {}

    if($('#actionFilter').val() !== '') {        
        filter.action = $('#actionFilter').val()        
    }

    const dateFilterIdx = $('#dateFilter').val();    

    if(dateFilterIdx !== '0') {
        var date = new Date();

        if(dateFilterIdx === '2'){
            date.setDate(date.getDate() + 2)
        } else if(dateFilterIdx === '3'){
            date.setDate(date.getDate() + 14)
        }

        let dateFilter = date.getFullYear() + '-' 
            + ("0" + (date.getMonth()+1)).slice(-2) + '-'
            + ("0" + date.getDate()).slice(-2);
        
        filter.date_lte = dateFilter;      
    }
    
    return filter;
}

function retrieve_data(filter){
    $.ajax({
        url: 'https://my-json-server.typicode.com/nebojsazr/todo_service/todos',
        method: 'GET',
        data: filter,
        dataType: 'json'
    }).done(function(data) {
        if (data.length > 0) {
            for(i=0; i<data.length; i++) {
                // console.log('element ' + i, data[i]);
                data[i].date = new Date( data[i].date );
                createListItem( data[i] );
            }
        }
    }).fail();
}


function add_new() {
    // selektovati element  u kojem se nalazi forma
    const forma = document.querySelector('.cont_crear_new');

    // Dodavanje ili oduzimanje css klase koja cini da forma postane vidljiva
    forma.classList.toggle('cont_crear_new_active');
}


function add_to_list() {
    
    const deadline = new Date(document.getElementById('deadline').value);
    // POdatke iz elemenata forme prebacujemo u promenljive da bismo ih kasnije iskoristili
    const elements = {
        action: document.getElementById('action_select'),
        title: document.querySelector('.input_title_desc'),
        date: deadline,
        desc: document.querySelector('.input_description')
    }

    if (!isValid(elements)) {
        return false;
    }

    createListItem({
        action: elements.action.value,
        title: elements.title.value,
        date: elements.date,
        desc: elements.desc.value
    });

}

function createListItem( itemData ) {
    // Kreiramo node za postojeci html element u koji cemo dodavati nove elemente (planove za uraditi)
    const ul = document.querySelector('.cont_princ_lists > ul');

    // Ovde cuvamo koliko child nodova (li elemenata trenutno ima u listi)
    const childNum = ul.children.length;

    // Krecemo sa kreiranjem novog child noda koji se dodaje prilikom jednoj izvrsavanja ove funkcije

    // Kreiramo osnovni child node element
    const li = document.createElement('li');
    // i dodajemo mu css klasu
    li.classList.add('list_' + itemData.action.toLowerCase());

    // dodajemo mu jos jednu klasu koja sadrzi redni broj elementa
    // Pomocu ove klase mozemo jedinstveno identifikovati element
    const itemClass = 'li_num_0_' + (childNum + 1);
    li.classList.add( itemClass );

    // Create header
    const itemHeader = document.createElement('div');
    itemHeader.classList.add('item-header');
    li.appendChild(itemHeader);

    // Create leftTimeContainer
    const leftTimeContainer = document.createElement('span');
    leftTimeContainer.classList.add('left-time-container');
    let leftTimeContainerText = 'Time left: ';
    let leftTime = leftUntil( itemData.date );
    leftTimeContainerText += `${leftTime.months} meseci 
                                ${leftTime.days} dana
                                ${leftTime.hours} sati
                                ${leftTime.minutes} minuta
                                ${leftTime.seconds} sekndi`;

    leftTimeContainer.textContent = leftTimeContainerText;
    itemHeader.appendChild(leftTimeContainer);

    // Kreiramo div koji predstavlja prvu kolonu i sadrzi podatak 'action' iz forme;
    const div1 = document.createElement('div');
    div1.className = 'col_md_1_list';          // Dodajemo mu njegovu klasu
    div1.innerHTML = `<p>${itemData.action}</p>`;     // i dodajemo mu njegov sadržaj

    // Kreiramo div koji predstavlja drugu kolonu i sadrzi podatke 'title' i 'description' iz forme;
    const div2 = document.createElement('div');
    div2.className = 'col_md_2_list';             // dodajemo mu njegovu klasu

    // Kreiramo pod element title kao node da bismo mogli da operisemo sa njim
    const div2Title = document.createElement('h4');
    div2Title.textContent = itemData.title;
    
    // Kreiramo pod element za description kao node da bismo mogli da operisemo sa njim
    const div2Desc = document.createElement('p');
    div2Desc.classList.add(`desc${childNum+1}`);    // Dodajemo mu jedinstvenu klasu koristeci redni broj
    div2Desc.textContent = itemData.desc;                   // i dodajemo mu text content

    // Dodajemo novokreirane podelemente u drugu kolonu
    div2.appendChild(div2Title);
    div2.appendChild(div2Desc);

    // Kreiramo event listener za click event nad elementom koji predstavlja title
    div2Title.addEventListener('click', function() {
        let desc  = document.getElementsByClassName('desc' + (childNum + 1))[0];  // Selektujemo description element sa odredjenim rednim brojem
        desc.classList.toggle('hidden');                                          // Sakrivamo descritopn element koristeci css classu hidden
    });

    // Kreiramo div koji predstavlja trecu kolonu i sadrzi podatak 'date' iz forme;
    const div3 = document.createElement('div');
    // idodajemo mu njegovu css klasu
    div3.className = 'col_md_3_list';
    div3.innerHTML = '<div class="cont_text_date"><p>' + itemData.date.toLocaleDateString() + '</p></div>';

    // Kreiramo button elemenent koji ce se koristiti za brisanje tekuceg child elementa od liste 
    const deleteBtn = document.createElement('button');
    deleteBtn.textContent = 'X';          // i dodajemo mu text

    // nad kreiranim buttonnom kreiramo event listener koji brise tekuci li element iz liste
    deleteBtn.addEventListener('click', function () {
        if(confirm('Da li ste sigurni da zelite da obrisete ovo')) {
            ul.removeChild(li); // brisanje itema
        }
    });
    
    // Dodajemo sve kreirane segmente u glavni li child node
    li.appendChild(div1);
    li.appendChild(div2);
    li.appendChild(div3);
    itemHeader.appendChild(deleteBtn);

    // I konacno dodati kreirani node u document tako da se on pojavljuje u ovom trenutku
    ul.appendChild(li);
    
    // Nakon sto je novi element kreiran i dodan, imalo bi smisla da se elementi forme resetuju
    // Stoga postavljamo njihove vrednosti na pocetno stanje
    document.getElementById('action_select').value = 'SHOPPING';
    document.querySelector('.input_title_desc').value = '';
    // document.getElementById('date_select').value = 'TODAY';
    document.querySelector('.input_description').value = '';
}

function isValid(elems) {

    elems.title.classList.remove('invalid');
    elems.desc.classList.remove('invalid');
    

    let result = true;
    if( String(elems.title.value).replace(' ', '') == '') {
        // Uokviriti input
        elems.title.classList.add('invalid');
        result = false;
    }

    if( String(elems.desc.value).trim() == '') {
        // Uokviriti input
        elems.desc.classList.add('invalid');
        result = false;
    }

    const now = new Date();
    if(now.getTime() >= elems.date.getTime()) {
        result = false;
    }



    return result
}